class CommentsController < ApplicationController
		before_action :authenticate_current_user, only: [:create, :upvote]

		def index
			post = Post.find(params[:post_id])
			respond_with post.comments
		end

		def create
				post = Post.find(params[:post_id])
				comment = post.comments.create(comment_params.merge(user_id: get_current_user.id, upvotes: 0))
				respond_with post, comment
		end

		def upvote
				post = Post.find(params[:post_id])
				comment = post.comments.find(params[:id])
				comment.increment!(:upvotes)
				respond_with comment
		end

		# Whitelisting :body strong param
		private
		def comment_params
				params.require(:comment).permit(:body)
		end
end
