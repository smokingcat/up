angular.module('flapperNews')
.directive('collapsible', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).collapsible();
        }
    };
});
