angular.module('flapperNews')
	.controller('MainCtrl', [
		'$scope',
		'posts',
		'$mdDialog',
		'$mdToast',
		function($scope, posts, $mdDialog, $mdToast) {
		 var dialogContent = ' \
				<md-toolbar class="md-primary"> \
					<h1 class="md-toolbar-tools"> \
					  Submit a link\
					</h1> \
			    </md-toolbar> \
				<md-content> \
					<md-input-container> \
						<label>Title</label> \
						<input ng-model="post.title" class="dialog-close" required="required"></input> \
					</md-input-container> \
					<md-input-container> \
						<label>Link</label> \
						<input ng-model="post.link" required="required"></input> \
					</md-input-container> \
				</md-content> \
				<div class="md-actions"> \
					<!-- type=button is needed so form uses submit button --> \
					<md-button type=button ng-click="cancel()">Cancel</md-button> \
					<md-button class="md-primary" type="submit" ng-click="hide()">Submit</md-button> \
				</div> \
			';
    		var formInside = '<md-dialog><form ng-submit="hide()">' + dialogContent + '</form></md-dialog>';
			function makeDialog(tmpl) {
				return function(ev) {
					$mdDialog.show({
						template: tmpl,
						targetEvent: ev,
						controller: 'DialogController'
					})
					.then(function(answer) {
						if (answer.title && answer.link)
						{
							$mdToast.show(
								$mdToast.simple()
								.content('link successfully added')
							);
							posts.create({
								title: answer.title,
								link: answer.link,
							});
						} else {
							$mdToast.show(
								$mdToast.simple()
								.content('You need to provide a title and a link')
							);

						}
					}, function() {
						$mdToast.show(
							$mdToast.simple()
							.content('cancelled')
						);
					});
				};
			}
			$scope.dialogFormInside = makeDialog(formInside);
			$scope.posts = posts.posts;
			$scope.incrementUpvotes = function (post) {
				posts.upvote(post);
			}
		}
	]);
angular.module('flapperNews')
   .controller('DialogController', ['$scope', '$mdDialog', function($scope, $mdDialog) {
    $scope.post = {
        title: '',
        link: '',
    };
    $scope.hide = function() {
        $mdDialog.hide($scope.post);
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
}]);

