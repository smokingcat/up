angular.module('flapperNews')
.controller('authCtrl', function($scope, $auth, $location, alertService, $mdToast) {
	// Registertation Purposes
	$scope.handleRegBtnClick = function() {
		$auth.submitRegistration($scope.registrationForm);
	};

	$scope.$on('auth:registration-email-success', function(ev, message) {
		$location.path('#/');
		$mdToast.show($mdToast.simple().content('Welcome! Please confirm your e-mail adress.'));
	});

	$scope.$on('auth:email-confirmation-success', function(ev, user) {
		$location.path('#/');
		$mdToast.show($mdToast.simple().content("Welcome, "+user.nickname+". Your account has been verified."));
	});

	// Login Pursposes
	$scope.handleLoginBtnClick = function() {
		$auth.submitLogin($scope.loginForm)
		.then(function(resp) { })
		.catch(function(resp) { });
	};

	$scope.$on('auth:login-success', function(ev, user) {
		$location.path('#/');
		$mdToast.show($mdToast.simple().content('Welcome ' + user.nickname + '!'));
	});

});
