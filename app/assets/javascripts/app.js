angular.module('flapperNews', ['ui.router', 'templates', 'ng-token-auth', 'ui.bootstrap', 'ngMaterial'])
	.config([
		'$stateProvider',
		'$urlRouterProvider',
		'$authProvider',
		'$mdThemingProvider',
		function($stateProvider, $urlRouterProvider, $authProvider, $mdThemingProvider) {
			$stateProvider
				.state('home', {
					url: '/home',
					templateUrl: 'home/_home.html',
					controller: 'MainCtrl',
					resolve: {
						  postPromise: ['posts', function(posts){
							      return posts.getAll();
						  }]
					}
				})
				.state('posts', {
					url: '/posts/{id}',
					templateUrl: 'posts/_posts.html',
					resolve: {
						post: ['$stateParams', 'posts', function($stateParams, posts) {
							return posts.get($stateParams.id);
						}],
					},
					controller: 'PostsCtrl'
				})
				.state('register', {
					url: '/register',
					templateUrl: 'auth/_register.html',
					controller: 'authCtrl'
				})
				.state('login', {
					url: '/login',
					templateUrl: 'auth/_login.html',
					controller: 'authCtrl'
				});
			$urlRouterProvider.otherwise('home');
			$authProvider.configure({
				apiUrl: 'http://localhost:3000'
			});
			$mdThemingProvider.theme('default')
				.primaryPalette('grey', {
					 'default': '800'
				})
    			.accentPalette('red')
		}
	]);
