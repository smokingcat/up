# Up+ (WIP) #
---

This is a reddit-style link sharing website.

People can share, comment and upvote links, which are then displayed on the front page accordingly.

I used this project as way to explore the rails/angular stack.

* back end: RoR
* front end: angular.js + Angular-Material
* auth: Devise_token_auth (token based authentication / authorization)
* db: sqlite3



### Oreview ###
Homepage
![homepage.png](http://i.imgur.com/Y1TCwe8.png)

Registering
![registering.png](http://i.imgur.com/ZLgPUGm.png)

Commenting
![commenting.png](http://i.imgur.com/tvfuucA.png)