Rails.application.routes.draw do
  	mount_devise_token_auth_for 'User', at: 'auth'
		root 'application#angular'

		resources :posts, defaults: {format: :json}, only: [:create, :index, :show] do
				resources :comments, defaults: {format: :json}, only: [:show, :index,:create] do
						member do
								put '/upvote' => 'comments#upvote'
						end
				end
				member do
						put '/upvote' => 'posts#upvote'
				end
		end
end
